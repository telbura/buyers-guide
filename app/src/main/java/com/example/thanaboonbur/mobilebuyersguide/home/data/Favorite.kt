package com.example.thanaboonbur.mobilebuyersguide.home.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by thanaboon.bur on 22/5/2018 AD 18:15.
 */
@Entity
data class Favorite(
        @PrimaryKey val id: Int,
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "rating") val rating: Double,
        @ColumnInfo(name = "thumbImageURL") val thumbImageURL: String,
        @ColumnInfo(name = "price") val price: Double,
        val description: String
)