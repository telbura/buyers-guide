package com.example.thanaboonbur.mobilebuyersguide.base.view

import android.support.annotation.StringRes

/**
 * Created by thanaboon.bur on 22/5/2018 AD 11:40.
 */
interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun showToast(message: String)
}