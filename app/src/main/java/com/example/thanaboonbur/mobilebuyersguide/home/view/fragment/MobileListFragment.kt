package com.example.thanaboonbur.mobilebuyersguide.home.view.fragment

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.thanaboonbur.mobilebuyersguide.R
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.MobileListPresenter
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract.MobileListContract
import com.example.thanaboonbur.mobilebuyersguide.home.view.adapter.MobileListAdapter
import com.example.thanaboonbur.mobilebuyersguide.util.GlobalVar
import kotlinx.android.synthetic.main.fragment_mobile_list.view.*

/**
 * Created by thanaboon.bur on 21/5/2018 AD 19:07.
 */
class MobileListFragment: Fragment(), MobileListContract.View {

    private var mAdapter: MobileListAdapter? = null
    private var mobileList: MutableList<Mobile> = mutableListOf()
    private val presenter: MobileListContract.Presenter = MobileListPresenter()
    private var progressBarLayout: RelativeLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.attach(this)
        presenter.init(mobileList)
        mAdapter = MobileListAdapter(context, presenter)
        val mLayoutManager = LinearLayoutManager(context)
        val view = inflater.inflate(R.layout.fragment_mobile_list, container, false)
        progressBarLayout = view.layout_progress_bar
        view.mobile_list_recyclerview.layoutManager = mLayoutManager
        view.mobile_list_recyclerview.itemAnimator = DefaultItemAnimator()
        view.mobile_list_recyclerview.adapter = mAdapter
        getMobileList()
        return view
    }

    internal fun getMobileList() {
        when(GlobalVar.SORT) {
            0 -> context?.let { presenter.getMobileListByPriceAsc(MyDatabase.getAppDatabase(it), isNetworkAvailable()) }
            1 -> context?.let { presenter.getMobileListByPriceDesc(MyDatabase.getAppDatabase(it), isNetworkAvailable()) }
            2 -> context?.let { presenter.getMobileListByRating(MyDatabase.getAppDatabase(it), isNetworkAvailable()) }
            else -> context?.let { presenter.getMobileListByPriceAsc(MyDatabase.getAppDatabase(it), isNetworkAvailable()) }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (this.isVisible) {
            mAdapter?.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clear()
        presenter.detach()
    }

    override fun onGetMobileListSuccess(mobileList: List<Mobile>) {
        this.mobileList.clear()
        this.mobileList.addAll(mobileList)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onGetMobileListFail(throwable: Throwable) {
        Log.d("fail", "fail")
    }

    override fun showProgress() {
        progressBarLayout?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBarLayout?.visibility = View.GONE
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}