package com.example.thanaboonbur.mobilebuyersguide.base.model

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by thanaboon.bur on 22/5/2018 AD 11:44.
 */
abstract class BaseModel {
    protected val mCompositeDisposable by lazy { CompositeDisposable() }
    fun clear() = mCompositeDisposable.clear()

    fun <T> Observable<T>.callAPI(observerWrapper: ObserverWrapper<T>): Disposable {
        return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(observerWrapper)
    }
}