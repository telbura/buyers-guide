package com.example.thanaboonbur.mobilebuyersguide.database

import android.arch.persistence.room.*
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile

/**
 * Created by thanaboon.bur on 22/5/2018 AD 18:10.
 */
@Dao
interface MobileDao {

    @get:Query("SELECT * FROM Mobile")
    val all: List<Mobile>

    @Query("SELECT * FROM Mobile ORDER BY price ASC")
    fun getAllByPriceAsc(): List<Mobile>

    @Query("SELECT * FROM Mobile ORDER BY price DESC")
    fun getAllByPriceDesc(): List<Mobile>

    @Query("SELECT * FROM Mobile ORDER BY rating DESC")
    fun getAllByRating(): List<Mobile>

    @Query("SELECT * FROM Mobile WHERE id = :mobile_id")
    fun getMobileDetail(mobile_id: Int): Mobile

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(mobileList: List<Mobile>)

    @Insert
    fun insert(mobile: Mobile): Long

    @Update
    fun update(mobile: Mobile)

    @Delete
    fun delete(mobile: Mobile)

}