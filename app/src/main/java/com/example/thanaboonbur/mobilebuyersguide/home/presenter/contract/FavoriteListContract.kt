package com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract

import com.example.thanaboonbur.mobilebuyersguide.base.presenter.BasePresenter
import com.example.thanaboonbur.mobilebuyersguide.base.view.BaseView
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase

/**
 * Created by thanaboon.bur on 22/5/2018 AD 12:07.
 */
interface FavoriteListContract {

    interface AdapterView {
        fun setName(name: String)
        fun setDescription(description: String)
        fun setRating(rating: String)
        fun setThumbImage(imageUrl: String)
    }

    interface View: BaseView {
        fun onGetFavoriteList(favoriteList: List<Favorite>)
        fun onRemoveFavorite(isEnable: Boolean)
    }

    abstract class Presenter: BasePresenter<FavoriteListContract.View>() {
        abstract fun init(favoriteList: List<Favorite>)
        abstract fun getFavoriteListByPriceAsc(db: MyDatabase)
        abstract fun getFavoriteListByPriceDesc(db: MyDatabase)
        abstract fun getFavoriteListByRating(db: MyDatabase)
        abstract fun getListSize(): Int
        abstract fun onBindViewHolder(db: MyDatabase, view: AdapterView, position: Int)
        abstract fun getMobileID(position: Int): Int
        abstract fun getDescription(position: Int): String
        abstract fun removeFavorite(db: MyDatabase, position: Int)
    }

}