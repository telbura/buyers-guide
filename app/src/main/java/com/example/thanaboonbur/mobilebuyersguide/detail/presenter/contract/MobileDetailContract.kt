package com.example.thanaboonbur.mobilebuyersguide.detail.presenter.contract

import com.example.thanaboonbur.mobilebuyersguide.base.presenter.BasePresenter
import com.example.thanaboonbur.mobilebuyersguide.base.view.BaseView
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.detail.data.MobileImage
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile

/**
 * Created by thanaboon.bur on 23/5/2018 AD 01:52.
 */
class MobileDetailContract {

    interface AdapterView {
        fun setImage(imageUrl: String)
    }

    interface View: BaseView {
        fun onGetMobileImageSuccess(mobileImageList: List<MobileImage>)
        fun onGetMobileImageFail(throwable: Throwable)
        fun onGetMobileDetail(mobile: Mobile)
    }

    abstract class Presenter: BasePresenter<MobileDetailContract.View>() {
        abstract fun init(mobileImageList: List<MobileImage>)
        abstract fun onBindViewHolder(db: MyDatabase, view: AdapterView, position: Int)
        abstract fun getMobileDetail(db: MyDatabase, mobile_id: Int)
        abstract fun getMobileImage(mobile_id: Int)
        abstract fun getListSize(): Int
    }
    
}