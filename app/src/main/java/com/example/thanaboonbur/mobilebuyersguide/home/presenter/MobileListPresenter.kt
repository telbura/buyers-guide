package com.example.thanaboonbur.mobilebuyersguide.home.presenter

import android.content.Context
import com.example.thanaboonbur.mobilebuyersguide.R
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.home.model.MobileModel
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract.MobileListContract
import android.net.NetworkInfo
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager



/**
 * Created by thanaboon.bur on 22/5/2018 AD 12:03.
 */
class MobileListPresenter: MobileListContract.Presenter() {

    private var mobileList: List<Mobile> = mutableListOf()
    private val mModel: MobileModel by lazy { MobileModel() }

    override fun init(mobileList: List<Mobile>) {
        this.mobileList = mobileList
    }

    override fun getListSize(): Int {
        return mobileList.size
    }

    override fun getMobileID(position: Int): Int {
        return mobileList[position].id
    }

    override fun getDescription(position: Int): String {
        return mobileList[position].description
    }

    override fun getMobileListByPriceAsc(db: MyDatabase, isOnline: Boolean) {
        mView?.showProgress()
        if(isOnline) {
            mModel.getMobileList(
                    {
                        db.mobileDao().insertAll(it)
                        mView?.hideProgress()
                        mView?.onGetMobileListSuccess(db.mobileDao().getAllByPriceAsc())
                    },
                    {
                        mView?.hideProgress()
                        mView?.onGetMobileListFail(it)
                    }
            )
        } else {
            mView?.hideProgress()
            mView?.onGetMobileListSuccess(db.mobileDao().getAllByPriceAsc())
        }
    }

    override fun getMobileListByPriceDesc(db: MyDatabase, isOnline: Boolean) {
        mView?.showProgress()
        if(isOnline) {
            mModel.getMobileList(
                    {
                        db.mobileDao().insertAll(it)
                        mView?.hideProgress()
                        mView?.onGetMobileListSuccess(db.mobileDao().getAllByPriceDesc())
                    },
                    {
                        mView?.hideProgress()
                        mView?.onGetMobileListFail(it)
                    }
            )
        } else {
            mView?.hideProgress()
            mView?.onGetMobileListSuccess(db.mobileDao().getAllByPriceDesc())
        }
    }

    override fun getMobileListByRating(db: MyDatabase, isOnline: Boolean) {
        mView?.showProgress()
        if(isOnline) {
            mModel.getMobileList(
                    {
                        db.mobileDao().insertAll(it)
                        mView?.hideProgress()
                        mView?.onGetMobileListSuccess(db.mobileDao().getAllByRating())
                    },
                    {
                        mView?.hideProgress()
                        mView?.onGetMobileListFail(it)
                    }
            )
        } else {
            mView?.hideProgress()
            mView?.onGetMobileListSuccess(db.mobileDao().getAllByRating())
        }
    }

    override fun toggleFavorite(db: MyDatabase, view: MobileListContract.AdapterView, position: Int, isEnable: Boolean) {
        mModel.toggleFavorite(
                db,
                Favorite(
                        mobileList[position].id,
                        mobileList[position].name,
                        mobileList[position].rating,
                        mobileList[position].thumbImageURL,
                        mobileList[position].price,
                        mobileList[position].description
                        ),
                isEnable,
                {
                    if(it){
                        view.setFavoriteIcon(R.drawable.heart_enable)
                        view.setEnableIcon(true)
                    } else {
                        view.setFavoriteIcon(R.drawable.heart_disable)
                        view.setEnableIcon(false)
                    }
                }
        )
    }

    override fun onBindViewHolder(db: MyDatabase, view: MobileListContract.AdapterView, position: Int) {
        view.setName(mobileList[position].name)
        view.setDescription(mobileList[position].description)
        view.setPrice(String.format("%.2f", mobileList[position].price))
        view.setRating(mobileList[position].rating.toString())
        view.setThumbImage(mobileList[position].thumbImageURL)
        if(db.favoriteDao().count(mobileList[position].id, mobileList[position].name) > 0) {
            view.setFavoriteIcon(R.drawable.heart_enable)
            view.setEnableIcon(true)
        } else {
            view.setFavoriteIcon(R.drawable.heart_disable)
            view.setEnableIcon(false)
        }
    }

    override fun clear() {
        mModel.clear()
    }
}