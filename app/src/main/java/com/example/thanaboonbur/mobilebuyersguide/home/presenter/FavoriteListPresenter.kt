package com.example.thanaboonbur.mobilebuyersguide.home.presenter

import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.home.model.FavoriteModel
import com.example.thanaboonbur.mobilebuyersguide.home.model.MobileModel
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract.FavoriteListContract

/**
 * Created by thanaboon.bur on 22/5/2018 AD 12:05.
 */
class FavoriteListPresenter: FavoriteListContract.Presenter() {

    private var favoriteList: List<Favorite> = mutableListOf()
    private val mModel: FavoriteModel by lazy { FavoriteModel() }
    private val mobileModel: MobileModel by lazy { MobileModel() }

    override fun init(favoriteList: List<Favorite>) {
        this.favoriteList = favoriteList
    }

    override fun getListSize(): Int {
        return favoriteList.size
    }

    override fun getMobileID(position: Int): Int {
        return favoriteList[position].id
    }

    override fun getDescription(position: Int): String {
        return favoriteList[position].description
    }

    override fun getFavoriteListByPriceAsc(db: MyDatabase) {
        mModel.getFavoriteListByPriceAsc(
                db,
                { mView?.onGetFavoriteList(it) })
    }

    override fun getFavoriteListByPriceDesc(db: MyDatabase) {
        mModel.getFavoriteListByPriceDesc(
                db,
                { mView?.onGetFavoriteList(it) })
    }

    override fun getFavoriteListByRating(db: MyDatabase) {
        mModel.getFavoriteListByRating(
                db,
                { mView?.onGetFavoriteList(it) })
    }

    override fun removeFavorite(db: MyDatabase, position: Int) {
        mobileModel.toggleFavorite(
                db,
                favoriteList[position],
                true,
                { mView?.onRemoveFavorite(it) }
        )
    }

    override fun onBindViewHolder(db: MyDatabase, view: FavoriteListContract.AdapterView, position: Int) {
        view.setName(favoriteList[position].name)
        view.setDescription(String.format("%.2f", favoriteList[position].price))
        view.setRating(favoriteList[position].rating.toString())
        view.setThumbImage(favoriteList[position].thumbImageURL)
    }

    override fun clear() {
        mModel.clear()
        mobileModel.clear()
    }

}