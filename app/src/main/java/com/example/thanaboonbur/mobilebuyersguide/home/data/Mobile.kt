package com.example.thanaboonbur.mobilebuyersguide.home.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by thanaboon.bur on 21/5/2018 AD 23:54.
 */
@Entity
data class Mobile(
        @PrimaryKey val id: Int,
        @ColumnInfo(name = "rating") val rating: Double,
        @ColumnInfo(name = "description") val description: String,
        @ColumnInfo(name = "thumbImageURL") val thumbImageURL: String,
        @ColumnInfo(name = "brand") val brand: String,
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "price") val price: Double
    )