package com.example.thanaboonbur.mobilebuyersguide.base.presenter

import com.example.thanaboonbur.mobilebuyersguide.base.view.BaseView

/**
 * Created by thanaboon.bur on 22/5/2018 AD 11:41.
 */

abstract class BasePresenter<V: BaseView> {

    protected var mView: V? = null

    open fun attach(view: V?) {
        view?.let {
            mView = view
        }
    }

    fun detach() {
        clear()
        mView = null
    }

    abstract fun clear()

    open fun onViewCreate() {}

    open fun onViewStart() {}

    open fun onViewResume() {}

    open fun onViewPause() {}

    open fun onViewStop() {}

    open fun onViewRestart() {}

    open fun onViewDestroy() {}
}
