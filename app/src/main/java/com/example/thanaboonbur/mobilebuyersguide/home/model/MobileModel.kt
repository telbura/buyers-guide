package com.example.thanaboonbur.mobilebuyersguide.home.model

import com.example.thanaboonbur.mobilebuyersguide.base.model.BaseModel
import com.example.thanaboonbur.mobilebuyersguide.base.model.ObserverWrapper
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.api.ApiClient
import com.example.thanaboonbur.mobilebuyersguide.detail.data.MobileImage

/**
 * Created by thanaboon.bur on 22/5/2018 AD 12:25.
 */
class MobileModel : BaseModel() {

    fun getMobileList(onSuccess: (List<Mobile>) -> Unit,
                      onFailed: (Throwable) -> Unit,
                      onComplete: () -> Unit = {}) {
        try {
            val apiClient = ApiClient()
            val mobileApi = apiClient.getMobileApi()
            if (mobileApi != null) {
                mCompositeDisposable.add(
                        mobileApi.getMobileList()
                                .callAPI(ObserverWrapper(
                                        {
                                            onSuccess(it)
                                        },
                                        {
                                            onFailed(it)
                                        }
                                ))
                )
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getMobileImage(mobile_id: Int, onSuccess: (List<MobileImage>) -> Unit,
                       onFailed: (Throwable) -> Unit,
                       onComplete: () -> Unit = {}) {
        try {
            val apiClient = ApiClient()
            val mobileApi = apiClient.getMobileApi()
            if (mobileApi != null) {
                mCompositeDisposable.add(
                        mobileApi.getMobileImage(mobile_id)
                                .callAPI(ObserverWrapper(
                                        {
                                            onSuccess(it)
                                        },
                                        {
                                            onFailed(it)
                                        }
                                ))
                )
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun toggleFavorite(db: MyDatabase, favorite: Favorite, isEnable: Boolean, callback: (Boolean) -> Unit = {}) {
        if(!isEnable) {
            db.favoriteDao().insert(favorite)
            callback(true)
        } else {
            db.favoriteDao().delete(favorite)
            callback(false)
        }
    }

}