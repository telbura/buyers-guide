package com.example.thanaboonbur.mobilebuyersguide.home.model

import com.example.thanaboonbur.mobilebuyersguide.base.model.BaseModel
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase

/**
 * Created by thanaboon.bur on 22/5/2018 AD 20:42.
 */
class FavoriteModel : BaseModel() {

    fun getFavoriteListByPriceAsc(db: MyDatabase, callback: (List<Favorite>) -> Unit) {
        try {
            callback(db.favoriteDao().getAllByPriceAsc())
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getFavoriteListByPriceDesc(db: MyDatabase, callback: (List<Favorite>) -> Unit) {
        try {
            callback(db.favoriteDao().getAllByPriceDesc())
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getFavoriteListByRating(db: MyDatabase, callback: (List<Favorite>) -> Unit) {
        try {
            callback(db.favoriteDao().getAllByRating())
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}