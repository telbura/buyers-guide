package com.example.thanaboonbur.mobilebuyersguide.detail.model

import com.example.thanaboonbur.mobilebuyersguide.base.model.BaseModel
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile

/**
 * Created by thanaboon.bur on 23/5/2018 AD 01:59.
 */
class MobileDetailModel: BaseModel() {

    fun getMobileDetail(db: MyDatabase, mobile_id: Int, callback: (Mobile) -> Unit) {
        callback(db.mobileDao().getMobileDetail(mobile_id))
    }

}