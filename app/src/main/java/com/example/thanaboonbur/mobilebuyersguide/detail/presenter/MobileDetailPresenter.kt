package com.example.thanaboonbur.mobilebuyersguide.detail.presenter

import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.detail.data.MobileImage
import com.example.thanaboonbur.mobilebuyersguide.detail.model.MobileDetailModel
import com.example.thanaboonbur.mobilebuyersguide.detail.presenter.contract.MobileDetailContract
import com.example.thanaboonbur.mobilebuyersguide.home.model.MobileModel

/**
 * Created by thanaboon.bur on 23/5/2018 AD 01:58.
 */
class MobileDetailPresenter: MobileDetailContract.Presenter() {

    private var mobileImageList: List<MobileImage> = mutableListOf()
    private val mModel: MobileDetailModel by lazy { MobileDetailModel() }
    private val mobileModel: MobileModel by lazy { MobileModel() }

    override fun init(mobileImageList: List<MobileImage>) {
        this.mobileImageList = mobileImageList
    }

    override fun getMobileDetail(db: MyDatabase, mobile_id: Int) {
        mModel.getMobileDetail(
                db,
                mobile_id,
                { mView?.onGetMobileDetail(it) }
        )
    }

    override fun getMobileImage(mobile_id: Int) {
        mobileModel.getMobileImage(
                mobile_id,
                { mView?.onGetMobileImageSuccess(it) },
                { mView?.onGetMobileImageFail(it) }
                )
    }

    override fun onBindViewHolder(db: MyDatabase, view: MobileDetailContract.AdapterView, position: Int) {
        view.setImage(mobileImageList[position].url)
    }

    override fun getListSize(): Int {
        return mobileImageList.size
    }

    override fun clear() {
        mModel.clear()
    }
}