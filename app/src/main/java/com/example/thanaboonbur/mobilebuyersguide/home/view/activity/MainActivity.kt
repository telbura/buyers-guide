package com.example.thanaboonbur.mobilebuyersguide.home.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.thanaboonbur.mobilebuyersguide.R
import android.support.v4.view.ViewPager
import com.example.thanaboonbur.mobilebuyersguide.home.view.adapter.ViewPagerAdapter
import com.example.thanaboonbur.mobilebuyersguide.home.view.fragment.FavoriteListFragment
import com.example.thanaboonbur.mobilebuyersguide.home.view.fragment.MobileListFragment
import kotlinx.android.synthetic.main.activity_main.*
import android.view.Menu
import android.view.MenuItem
import android.support.v7.app.AlertDialog
import com.example.thanaboonbur.mobilebuyersguide.util.GlobalVar


class MainActivity : AppCompatActivity() {

    private val mobileListFragment: MobileListFragment = MobileListFragment()
    private val favoriteListFragment: FavoriteListFragment = FavoriteListFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewPager(home_viewpager)
        home_tabs.setupWithViewPager(home_viewpager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(mobileListFragment, "MOBILE LIST")
        adapter.addFragment(favoriteListFragment, "FAVORITE LIST")
        viewPager.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == R.id.action_sort) {
            showDialog()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun showDialog() {
        val adb = AlertDialog.Builder(this)
        val items = arrayOf<CharSequence>("Price low to high", "Price high to low", "Rating")
        adb.setSingleChoiceItems(items, GlobalVar.SORT) { _, n ->
            GlobalVar.SORT = n
            mobileListFragment.getMobileList()
            favoriteListFragment.getFavoriteList()
        }
        adb.setNegativeButton("Cancel", null)
        adb.show()
    }

}
