package com.example.thanaboonbur.mobilebuyersguide.detail.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.PrimaryKey

/**
 * Created by thanaboon.bur on 23/5/2018 AD 17:19.
 */
data class MobileImage (
        val url: String,
        val id: Int,
        val mobile_id: Int
)