package com.example.thanaboonbur.mobilebuyersguide.base.model

import io.reactivex.observers.DisposableObserver

/**
 * Created by thanaboon.bur on 22/5/2018 AD 11:52.
 */
typealias OnSuccess<T> = (response: T) -> Unit
typealias OnFailed = (throwable: Throwable) -> Unit
typealias OnCompleted = () -> Unit

open class ObserverWrapper<T>(private var onSuccess: OnSuccess<T>,
                              private var onFailed: OnFailed,
                              private var onCompleted: OnCompleted = {}): DisposableObserver<T>(){

    override fun onNext(response: T) {
        try {
            onSuccess(response)
        } catch (e: Exception) {
            onFailed(e)
        }
    }

    override fun onError(throwable: Throwable) {
        onFailed(throwable)
    }

    override fun onComplete() {
        onCompleted()
    }
}