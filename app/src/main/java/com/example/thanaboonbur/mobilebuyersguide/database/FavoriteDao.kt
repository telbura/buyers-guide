package com.example.thanaboonbur.mobilebuyersguide.database

import android.arch.persistence.room.*
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile

/**
 * Created by thanaboon.bur on 22/5/2018 AD 18:18.
 */
@Dao
interface FavoriteDao {

    @get:Query("SELECT * FROM Favorite")
    val all: List<Favorite>

    @Query("SELECT * FROM Favorite ORDER BY price ASC")
    fun getAllByPriceAsc(): List<Favorite>

    @Query("SELECT * FROM Favorite ORDER BY price DESC")
    fun getAllByPriceDesc(): List<Favorite>

    @Query("SELECT * FROM Favorite ORDER BY rating DESC")
    fun getAllByRating(): List<Favorite>

    @Query("SELECT count(*) FROM Favorite WHERE id = :id AND name = :name")
    fun count(id: Int, name: String): Int

    @Insert
    fun insert(favorite: Favorite): Long

    @Update
    fun update(favorite: Favorite)

    @Delete
    fun delete(favorite: Favorite)

}