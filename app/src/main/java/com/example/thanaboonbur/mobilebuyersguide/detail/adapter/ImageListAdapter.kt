package com.example.thanaboonbur.mobilebuyersguide.detail.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.thanaboonbur.mobilebuyersguide.R
import com.example.thanaboonbur.mobilebuyersguide.R.id.iv_image
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.detail.presenter.contract.MobileDetailContract
import com.example.thanaboonbur.mobilebuyersguide.detail.view.activity.MobileDetailActivity
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract.MobileListContract
import kotlinx.android.synthetic.main.item_row_image_list.view.*
import kotlinx.android.synthetic.main.item_row_mobile_list.view.*


/**
 * Created by thanaboon.bur on 21/5/2018 AD 22:55.
 */
class ImageListAdapter(private val context: Context?, private val mPresenter: MobileDetailContract.Presenter): RecyclerView.Adapter<ImageListAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), MobileDetailContract.AdapterView {

        private val ivImage: ImageView = view.iv_image

        override fun setImage(imageUrl: String) {
            var url = imageUrl
            if(!url.startsWith("http://") && !url.startsWith("https://")) {
                url = "http://$url"
            }
            try {
                context?.let { Glide.with(context).load(url).into(ivImage); }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val convertView = LayoutInflater.from(context).inflate(R.layout.item_row_image_list, null)
        return ViewHolder(convertView)
    }

    override fun getItemCount(): Int {
        return mPresenter.getListSize()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        context?.let { mPresenter.onBindViewHolder(MyDatabase.getAppDatabase(it), holder, position) }
    }

}