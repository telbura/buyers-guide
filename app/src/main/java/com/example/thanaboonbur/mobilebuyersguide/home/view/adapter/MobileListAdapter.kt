package com.example.thanaboonbur.mobilebuyersguide.home.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.thanaboonbur.mobilebuyersguide.R
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.detail.view.activity.MobileDetailActivity
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract.MobileListContract
import kotlinx.android.synthetic.main.item_row_mobile_list.view.*


/**
 * Created by thanaboon.bur on 21/5/2018 AD 22:55.
 */
class MobileListAdapter(private val context: Context?, private val mPresenter: MobileListContract.Presenter): RecyclerView.Adapter<MobileListAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), MobileListContract.AdapterView {

        private val ivThumb: ImageView = view.iv_thumbimage
        val ivFavorite: ImageView = view.iv_favorite
        private val tvName: TextView = view.tv_name
        private val tvDescription: TextView = view.tv_description
        private val tvPrice: TextView = view.tv_price
        private val tvRating: TextView = view.tv_rating
        val cardView: CardView = view.card_view
        var isEnable: Boolean = false

        override fun setThumbImage(imageUrl: String) {
            try {
                context?.let { Glide.with(context).load(imageUrl).into(ivThumb); }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun setEnableIcon(enable: Boolean) {
            isEnable = enable
        }

        override fun setName(name: String) {
            tvName.text = name
        }

        override fun setDescription(description: String) {
            tvDescription.text = description
        }

        override fun setPrice(price: String) {
            tvPrice.text = price
        }

        override fun setRating(rating: String) {
            tvRating.text = rating
        }

        override fun setFavoriteIcon(drawable: Int) {
            ivFavorite.setImageResource(drawable)
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val convertView = LayoutInflater.from(context).inflate(R.layout.item_row_mobile_list, null)
        return ViewHolder(convertView)
    }

    override fun getItemCount(): Int {
        return mPresenter.getListSize()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        context?.let { mPresenter.onBindViewHolder(MyDatabase.getAppDatabase(it), holder, position) }
        holder.ivFavorite.setOnClickListener { _ ->
            context?.let {
                mPresenter.toggleFavorite(
                        MyDatabase.getAppDatabase(context),
                        holder,
                        position,
                        holder.isEnable)
            }
        }
        holder.cardView.setOnClickListener { _ ->
            context?.let {
                val intent = Intent(context, MobileDetailActivity::class.java)
                intent.putExtra("mobile_id", mPresenter.getMobileID(position))
                context.startActivity(intent)
            }
        }
    }

}