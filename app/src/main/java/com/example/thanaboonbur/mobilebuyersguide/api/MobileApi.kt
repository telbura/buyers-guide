package com.example.thanaboonbur.mobilebuyersguide.api

import android.support.annotation.Keep
import com.example.thanaboonbur.mobilebuyersguide.detail.data.MobileImage
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by thanaboon.bur on 22/5/2018 AD 14:07.
 */

@Keep
interface MobileApi {

    @GET(".")
    fun getMobileList() : Observable<List<Mobile>>

    @GET("{mobile_id}/images/")
    fun getMobileImage(@Path("mobile_id") mobile_id: Int) : Observable<List<MobileImage>>
}