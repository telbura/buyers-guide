package com.example.thanaboonbur.mobilebuyersguide.home.view.fragment

import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.thanaboonbur.mobilebuyersguide.R
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.FavoriteListPresenter
import com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract.FavoriteListContract
import com.example.thanaboonbur.mobilebuyersguide.home.view.adapter.FavoriteListAdapter
import com.example.thanaboonbur.mobilebuyersguide.util.GlobalVar
import kotlinx.android.synthetic.main.fragment_favorite_list.view.*
import kotlinx.android.synthetic.main.fragment_mobile_list.view.*

/**
 * Created by thanaboon.bur on 21/5/2018 AD 19:07.
 */
class FavoriteListFragment: Fragment(), FavoriteListContract.View {

    private var mAdapter: FavoriteListAdapter? = null
    private var favoriteList: MutableList<Favorite> = mutableListOf()
    private val presenter: FavoriteListContract.Presenter = FavoriteListPresenter()
    private var progressBarLayout: RelativeLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter.attach(this)
        presenter.init(favoriteList)
        mAdapter = FavoriteListAdapter(context, presenter)
        val mLayoutManager = LinearLayoutManager(context)
        val view = inflater.inflate(R.layout.fragment_favorite_list, container, false)
        progressBarLayout = view.layout_progress_bar
        view.favorite_list_recyclerview.layoutManager = mLayoutManager
        view.favorite_list_recyclerview.itemAnimator = DefaultItemAnimator()
        view.favorite_list_recyclerview.adapter = mAdapter
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(view.favorite_list_recyclerview)
        getFavoriteList()
        return view
    }

    private var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            //Remove swiped item from list and notify the RecyclerView
            val position = viewHolder.adapterPosition
            context?.let { presenter.removeFavorite(MyDatabase.getAppDatabase(it), position) }
        }
    }

    internal fun getFavoriteList() {
        when(GlobalVar.SORT) {
            0 -> context?.let { presenter.getFavoriteListByPriceAsc(MyDatabase.getAppDatabase(it)) }
            1 -> context?.let { presenter.getFavoriteListByPriceDesc(MyDatabase.getAppDatabase(it)) }
            2 -> context?.let { presenter.getFavoriteListByRating(MyDatabase.getAppDatabase(it)) }
            else -> context?.let { presenter.getFavoriteListByPriceAsc(MyDatabase.getAppDatabase(it)) }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (this.isVisible) {
            getFavoriteList()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clear()
        presenter.detach()
    }

    override fun onGetFavoriteList(favoriteList: List<Favorite>) {
        this.favoriteList.clear()
        this.favoriteList.addAll(favoriteList)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onRemoveFavorite(isEnable: Boolean) {
        getFavoriteList()
    }

    override fun showProgress() {
        progressBarLayout?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBarLayout?.visibility = View.GONE
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}