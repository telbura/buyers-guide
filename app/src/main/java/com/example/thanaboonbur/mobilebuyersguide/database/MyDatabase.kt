package com.example.thanaboonbur.mobilebuyersguide.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.thanaboonbur.mobilebuyersguide.home.data.Favorite
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile

/**
 * Created by thanaboon.bur on 22/5/2018 AD 18:09.
 */
@Database(entities = [Mobile::class, Favorite::class], version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun mobileDao(): MobileDao

    abstract fun favoriteDao(): FavoriteDao

    companion object {

        private var INSTANCE: MyDatabase? = null

        fun getAppDatabase(context: Context): MyDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, MyDatabase::class.java, "BusinessPlanner")
                        .allowMainThreadQueries()
                        .build()
            }
            return INSTANCE as MyDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}