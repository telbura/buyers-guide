package com.example.thanaboonbur.mobilebuyersguide.api

import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by thanaboon.bur on 22/5/2018 AD 12:26.
 */
class ApiClient {

    private val MOBILE_URL = "https://scb-test-mobile.herokuapp.com/api/mobiles/"

    fun getMobileApi(): MobileApi? {
        return if (MOBILE_URL.equals("", ignoreCase = true)) {
            null
        } else {
            provideRetrofit(MOBILE_URL, provideClient()).create(MobileApi::class.java)
        }
    }

    private fun provideClient(): OkHttpClient {
        val READ_TIMEOUT = 15
        val CONNECTION_TIMEOUT = 15

        // Add Logging
        val networkLayerLogger: HttpLoggingInterceptor.Logger = HttpLoggingInterceptor.Logger { message -> Log.i("ApiClient", message) }
        val loggingInterceptor = HttpLoggingInterceptor(networkLayerLogger)
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder().readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS).connectTimeout(CONNECTION_TIMEOUT.toLong(), TimeUnit.SECONDS)

        // Must add last to be able to log everything from other interceptor
        builder.addInterceptor(loggingInterceptor)

        return builder.build()
    }

    private fun provideRetrofit(baseURL: String, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseURL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }
}