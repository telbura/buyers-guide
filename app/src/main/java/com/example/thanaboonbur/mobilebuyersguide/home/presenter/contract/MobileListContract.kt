package com.example.thanaboonbur.mobilebuyersguide.home.presenter.contract

import com.example.thanaboonbur.mobilebuyersguide.base.presenter.BasePresenter
import com.example.thanaboonbur.mobilebuyersguide.base.view.BaseView
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase

/**
 * Created by thanaboon.bur on 22/5/2018 AD 12:07.
 */
interface MobileListContract {

    interface AdapterView {
        fun setName(name: String)
        fun setDescription(description: String)
        fun setPrice(price: String)
        fun setRating(rating: String)
        fun setFavoriteIcon(drawable: Int)
        fun setEnableIcon(enable: Boolean)
        fun setThumbImage(imageUrl: String)
    }

    interface View: BaseView {
        fun onGetMobileListSuccess(mobileList: List<Mobile>)
        fun onGetMobileListFail(throwable: Throwable)
    }

    abstract class Presenter: BasePresenter<MobileListContract.View>() {
        abstract fun init(mobileList: List<Mobile>)
        abstract fun getMobileListByPriceAsc(db: MyDatabase, isOnline: Boolean)
        abstract fun getMobileListByPriceDesc(db: MyDatabase, isOnline: Boolean)
        abstract fun getMobileListByRating(db: MyDatabase, isOnline: Boolean)
        abstract fun toggleFavorite(db: MyDatabase, view: MobileListContract.AdapterView, position: Int, isEnable: Boolean)
        abstract fun onBindViewHolder(db: MyDatabase, view: AdapterView, position: Int)
        abstract fun getListSize(): Int
        abstract fun getMobileID(position: Int): Int
        abstract fun getDescription(position: Int): String
    }

}