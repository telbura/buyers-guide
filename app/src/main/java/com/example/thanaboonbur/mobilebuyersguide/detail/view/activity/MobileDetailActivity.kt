package com.example.thanaboonbur.mobilebuyersguide.detail.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.example.thanaboonbur.mobilebuyersguide.R
import com.example.thanaboonbur.mobilebuyersguide.database.MyDatabase
import com.example.thanaboonbur.mobilebuyersguide.detail.adapter.ImageListAdapter
import com.example.thanaboonbur.mobilebuyersguide.detail.data.MobileImage
import com.example.thanaboonbur.mobilebuyersguide.detail.presenter.MobileDetailPresenter
import com.example.thanaboonbur.mobilebuyersguide.detail.presenter.contract.MobileDetailContract
import com.example.thanaboonbur.mobilebuyersguide.home.data.Mobile
import kotlinx.android.synthetic.main.activity_mobile_detail.*

class MobileDetailActivity : AppCompatActivity(), MobileDetailContract.View {

    private val presenter: MobileDetailContract.Presenter = MobileDetailPresenter()
    private var mAdapter: ImageListAdapter? = null
    private var mobileImageList: MutableList<MobileImage> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        presenter.init(mobileImageList)
        setContentView(R.layout.activity_mobile_detail)
        mAdapter = ImageListAdapter(this, presenter)
        val mLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        image_recyclerview.layoutManager = mLayoutManager
        image_recyclerview.itemAnimator = DefaultItemAnimator()
        image_recyclerview.adapter = mAdapter
        layout_top.background.alpha = 150
        val mobileID = intent.getIntExtra("mobile_id", -1)
        if(mobileID != -1) {
            presenter.getMobileImage(mobileID)
            presenter.getMobileDetail(MyDatabase.getAppDatabase(this), mobileID)
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.clear()
        presenter.detach()
    }

    override fun onGetMobileDetail(mobile: Mobile) {
        tv_description.text = mobile.description
        tv_price.text = String.format("%.2f", mobile.price)
        tv_rating.text = mobile.rating.toString()
    }

    override fun onGetMobileImageSuccess(mobileImageList: List<MobileImage>) {
        this.mobileImageList.addAll(mobileImageList)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onGetMobileImageFail(throwable: Throwable) {

    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showToast(message: String) {
    }
}
